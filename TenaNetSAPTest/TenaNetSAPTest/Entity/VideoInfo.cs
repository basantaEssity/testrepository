﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TenaNetSAPTest.Entity
{
    public class VideoInfo
    {
        public int VideoId { get; set; }
        public string URL { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string VideoInfo { get; set; }
        public string VideoTag2 { get; set; }
        public string VideoTag3 { get; set; }
    }
}
