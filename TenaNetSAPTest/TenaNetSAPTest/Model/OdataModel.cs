﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TenaNetSAPTest.Model
{
    public class OdataModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

    }
}
